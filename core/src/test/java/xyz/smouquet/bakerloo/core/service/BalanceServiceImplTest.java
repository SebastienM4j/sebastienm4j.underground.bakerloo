package xyz.smouquet.bakerloo.core.service;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import xyz.smouquet.bakerloo.core.domain.Account;
import xyz.smouquet.bakerloo.core.domain.BankingOperation;
import xyz.smouquet.bakerloo.core.domain.Mode;
import xyz.smouquet.bakerloo.core.spi.BalanceRepository;
import xyz.smouquet.bakerloo.core.spi.BankingOperationRepository;

@RunWith(MockitoJUnitRunner.class)
public class BalanceServiceImplTest
{
    @Mock
    private BankingOperationRepository bankingOperationRepository;
    
    @InjectMocks
    private BalanceServiceImpl service;
    
    @Before
    public void setUp()
    {
        Optional<BalanceRepository> balanceRepository = Optional.empty();
        service.setBalanceRepository(balanceRepository);
    }
    
    
    @Test
    public void isValidBalanceOnAllOperations()
    {
        Long accountId = 1L;
        
        when(bankingOperationRepository.findAllByAccountId(accountId)).thenReturn(dataSet());
        
        BigDecimal result = service.balanceByAccountId(accountId);
        
        assertThat(result).isEqualByComparingTo(BigDecimal.valueOf(2980.38));
    }
    
    @Test
    public void isValidBalanceOnCheckedOperations()
    {
        Long accountId = 1L;
        
        when(bankingOperationRepository.findAllByAccountId(accountId)).thenReturn(dataSet());
        
        BigDecimal result = service.balanceCheckedOperationsByAccountId(accountId);
        
        assertThat(result).isEqualByComparingTo(BigDecimal.valueOf(3052.13));
    }
    
    @Test
    public void isValidBalanceOnUncheckedOperations()
    {
        Long accountId = 1L;
        
        when(bankingOperationRepository.findAllByAccountId(accountId)).thenReturn(dataSet());
        
        BigDecimal result = service.balanceUncheckedOperationsByAccountId(accountId);
        
        assertThat(result).isEqualByComparingTo(BigDecimal.valueOf(71.75).negate());
    }
    
    @Test
    public void isValidBalanceIfNoOperation()
    {
        Long accountId = 1L;
        
        when(bankingOperationRepository.findAllByAccountId(accountId)).thenReturn(new ArrayList<>());
        
        BigDecimal result = service.balanceByAccountId(accountId);
        
        assertThat(result).isEqualByComparingTo(BigDecimal.ZERO);
    }
    
    @Test
    public void isValidBalanceIfNullListOperations()
    {
        Long accountId = 1L;
        
        when(bankingOperationRepository.findAllByAccountId(accountId)).thenReturn(null);
        
        BigDecimal result = service.balanceByAccountId(accountId);
        
        assertThat(result).isEqualByComparingTo(BigDecimal.ZERO);
    }
    
    
    private List<BankingOperation> dataSet()
    {
        Account account = new Account();
        account.setId(1L);
        
        List<BankingOperation> operations = new ArrayList<>();
        operations.add(dataSetCreateOperation(account, LocalDate.of(2016, 6, 19), Mode.CREDIT_CARD, BigDecimal.valueOf(6.7), false));
        operations.add(dataSetCreateOperation(account, LocalDate.of(2016, 6, 19), Mode.CREDIT_CARD, BigDecimal.valueOf(45.05), false));
        operations.add(dataSetCreateOperation(account, LocalDate.of(2016, 6, 19), Mode.WITHDRAWAL_ATM, BigDecimal.valueOf(20.00), false));
        operations.add(dataSetCreateOperation(account, LocalDate.of(2016, 6, 5), Mode.CREDIT_CARD, BigDecimal.valueOf(89.99), true));
        operations.add(dataSetCreateOperation(account, LocalDate.of(2016, 6, 3), Mode.TRANSFER_RECEIVED, BigDecimal.valueOf(1905.89), true));
        operations.add(dataSetCreateOperation(account, LocalDate.of(2016, 6, 1), Mode.DEPOSIT_CHECK, BigDecimal.valueOf(1236.23), true));
        
        return operations;
    }
    
    private BankingOperation dataSetCreateOperation(Account account, LocalDate date, Mode mode, BigDecimal amount, boolean checked)
    {
        BankingOperation operation = new BankingOperation();
        operation.setAccount(account);
        operation.setDate(date);
        operation.setMode(mode);
        operation.setAmount(amount);
        operation.setChecked(checked);
        
        return operation;
    }
}
