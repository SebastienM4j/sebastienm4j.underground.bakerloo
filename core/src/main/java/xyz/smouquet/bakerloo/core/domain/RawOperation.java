package xyz.smouquet.bakerloo.core.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * A raw banking operation, not modified (not grouped, splitted, ...).
 * 
 * @author smouquet
 */
public class RawOperation extends UserOperation
{
    private BankingOperation operation;
    
    
    public RawOperation() {}

    
    public BankingOperation getOperation()
    {
        return operation;
    }
    
    public void setOperation(BankingOperation operation)
    {
        this.operation = operation;
    }
    
    @Override
    public Account getAccount()
    {
        return operation.getAccount();
    }

    @Override
    public void setAccount(Account account)
    {
        operation.setAccount(account);
    }

    @Override
    public LocalDate getDate()
    {
        return operation.getDate();
    }

    @Override
    public void setDate(LocalDate date)
    {
        operation.setDate(date);
    }

    @Override
    public Mode getMode()
    {
        return operation.getMode();
    }

    @Override
    public void setMode(Mode mode)
    {
        operation.setMode(mode);
    }

    @Override
    public String getThirdParty()
    {
        return operation.getThirdParty();
    }

    @Override
    public void setThirdParty(String thirdParty)
    {
        operation.setThirdParty(thirdParty);
    }
    
    @Override
    public String getReference()
    {
        return operation.getReference();
    }
    
    @Override
    public void setReference(String reference)
    {
        operation.setReference(reference);
    }

    @Override
    public BigDecimal getAmount()
    {
        return operation.getAmount();
    }

    @Override
    public void setAmount(BigDecimal amount)
    {
        operation.setAmount(amount);
    }

    @Override
    public Set<String> getTags()
    {
        return operation.getTags();
    }

    @Override
    public String getComment()
    {
        return operation.getComment();
    }

    @Override
    public void setComment(String comment)
    {
        operation.setComment(comment);
    }
    
}
