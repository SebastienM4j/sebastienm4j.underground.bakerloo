package xyz.smouquet.bakerloo.core.spi;

import java.io.Serializable;

/**
 * Repository for generic CRUD operations on specific type.
 *
 * @param <T> Concerned type
 * @param <ID> Entity id
 * 
 * @author smouquet
 */
public interface CrudRepository<T, ID extends Serializable>
{

    /**
     * Retrieve an entity by his id.
     * 
     * @param id The entity id (non <tt>null</tt>)
     * @return The found entity or <tt>null</tt>
     * @throws IllegalArgumentException If an argument is not valid
     */
    T find(ID id);
    
    /**
     * Save the entity.
     * 
     * <p>The implementation must not save in cascade,
     * this must be an unitary action. Cascading is under control of client.
     * 
     * @param entity Entity to save (non <tt>null</tt>)
     * @return The saved entity
     * @throws IllegalArgumentException If an argument is not valid
     */
    T save(T entity);
    
    /**
     * Delete the entity that correspond to this id.
     * 
     * <p>The implementation must not delete in cascade,
     * this must be an unitary action. Cascading is under control of client.
     * 
     * @param id The entity id (non <tt>null</tt>)
     * @throws IllegalArgumentException If an argument is not valid
     */
    void delete(ID id);
    
}
