package xyz.smouquet.bakerloo.core.api;

import xyz.smouquet.bakerloo.core.domain.Account;

/**
 * Service for {@link Account}.
 * 
 * @author smouquet
 */     
public interface AccountService extends CrudService<Account, Long>
{

}
