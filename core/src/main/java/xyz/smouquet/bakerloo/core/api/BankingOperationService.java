package xyz.smouquet.bakerloo.core.api;

import xyz.smouquet.bakerloo.core.domain.BankingOperation;

/**
 * Service for {@link BankingOperation}.
 * 
 * @author smouquet
 */ 
public interface BankingOperationService extends CrudService<BankingOperation, Long>
{

}
