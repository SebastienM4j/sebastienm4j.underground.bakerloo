package xyz.smouquet.bakerloo.core.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Represents a real banking operation (a real transaction).
 * 
 * This banking operation can be found in the state
 * on the account statement and can be checked.
 * 
 * @author smouquet
 */
public class BankingOperation implements Operation
{
    private Long id;
    
    private Account account;
    
    private LocalDate date;
    private Mode mode;
    private String thirdParty;
    private String reference;
    private BigDecimal amount;
    
    private Set<String> tags = new LinkedHashSet<>();
    private String comment;
    
    private boolean checked;
    
    
    public BankingOperation() {}

    
    @Override
    public Long getId()
    {
        return id;
    }
    
    @Override
    public void setId(Long id)
    {
        this.id = id;
    }
    
    @Override
    public Account getAccount()
    {
        return account;
    }

    @Override
    public void setAccount(Account account)
    {
        this.account = account;
    }

    @Override
    public LocalDate getDate()
    {
        return date;
    }

    @Override
    public void setDate(LocalDate date)
    {
        this.date = date;
    }

    @Override
    public Mode getMode()
    {
        return mode;
    }

    @Override
    public void setMode(Mode mode)
    {
        this.mode = mode;
    }

    @Override
    public String getThirdParty()
    {
        return thirdParty;
    }

    @Override
    public void setThirdParty(String thirdParty)
    {
        this.thirdParty = thirdParty;
    }
    
    @Override
    public String getReference()
    {
        return reference;
    }
    
    @Override
    public void setReference(String reference)
    {
        this.reference = reference;
    }

    @Override
    public BigDecimal getAmount()
    {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount)
    {
        this.amount = amount;
    }
    
    @Override
    public Set<String> getTags()
    {
        return tags;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    @Override
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    
    public boolean isChecked()
    {
        return checked;
    }
    
    public void setChecked(boolean checked)
    {
        this.checked = checked;
    }
    
}
