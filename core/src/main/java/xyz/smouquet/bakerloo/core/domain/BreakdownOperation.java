package xyz.smouquet.bakerloo.core.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A part of an banking operation.
 * 
 * A breakdown allows to define tags and comment
 * for a part of an operation amount. That can be 
 * interesting for statistical purpose.
 * 
 * @author smouquet
 */
public class BreakdownOperation extends UserOperation
{
    private BankingOperation operation;
    
    private BigDecimal amount;
    
    private Set<String> tags = new LinkedHashSet<>();
    private String comment;
    
    
    public BreakdownOperation() {}


    public BankingOperation getOperation()
    {
        return operation;
    }
    
    public void setOperation(BankingOperation operation)
    {
        this.operation = operation;
    }
    
    @Override
    public Account getAccount()
    {
        return operation.getAccount();
    }
    
    @Override
    public void setAccount(Account account)
    {
        operation.setAccount(account);
    }

    @Override
    public LocalDate getDate()
    {
        return operation.getDate();
    }
    
    @Override
    public void setDate(LocalDate date)
    {
        operation.setDate(date);
    }

    @Override
    public Mode getMode()
    {
        return operation.getMode();
    }
    
    @Override
    public void setMode(Mode mode)
    {
        operation.setMode(mode);
    }

    @Override
    public String getThirdParty()
    {
        return operation.getThirdParty();
    }
    
    @Override
    public void setThirdParty(String thirdParty)
    {
        operation.setThirdParty(thirdParty);
    }

    @Override
    public String getReference()
    {
        return operation.getReference();
    }
    
    @Override
    public void setReference(String reference)
    {
        operation.setReference(reference);
    }

    @Override
    public BigDecimal getAmount()
    {
        return this.amount;
    }

    @Override
    public void setAmount(BigDecimal amount)
    {
        this.amount = amount;
    }

    @Override
    public Set<String> getTags()
    {
        return this.tags;
    }

    @Override
    public String getComment()
    {
        return this.comment;
    }
    
    @Override
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    
}
