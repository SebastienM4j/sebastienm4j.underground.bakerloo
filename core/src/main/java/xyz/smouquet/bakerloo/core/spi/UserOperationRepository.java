package xyz.smouquet.bakerloo.core.spi;

import java.util.List;

import xyz.smouquet.bakerloo.core.domain.BankingOperation;
import xyz.smouquet.bakerloo.core.domain.UserOperation;

/**
 * Repository for {@link UserOperation}.
 * 
 * @author smouquet
 */
public interface UserOperationRepository extends CrudRepository<UserOperation, Long>
{

    /**
     * Retrives all {@link UserOperation} for an account
     * in a natural order of use.<br/>
     * <br/>
     * Operations are ordered by date and id (descending).
     * 
     * @param accountId Account id (non <tt>null</tt>)
     * @return All account user operations in a natural order of use
     * @throws IllegalArgumentException If an argument is not valid
     */
    List<UserOperation> findAllByAccountId(Long accountId);
    
    /**
     * Retrives all {@link UserOperation} derived from a {@link BankingOperation}.
     * 
     * @param bankingOperationId Banking operation id (non <tt>null</tt>)
     * @return All user operations
     * @throws IllegalArgumentException If an argument is not valid
     */
    List<UserOperation> findAllByBankingOperationId(Long bankingOperationId);
    
}
