package xyz.smouquet.bakerloo.core.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * Defines a banking operation for use of the application.
 * 
 * @author smouquet
 */
public interface Operation
{

    Long getId();
    
    void setId(Long id);
    
    Account getAccount();
    
    void setAccount(Account account);

    LocalDate getDate();
    
    void setDate(LocalDate date);

    Mode getMode();
    
    void setMode(Mode mode);

    String getThirdParty();
    
    void setThirdParty(String thirdParty);

    String getReference();
    
    void setReference(String reference);

    BigDecimal getAmount();
    
    void setAmount(BigDecimal amount);

    Set<String> getTags();

    String getComment();
    
    void setComment(String comment);

}