package xyz.smouquet.bakerloo.core.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Group of multiples banking operations.
 * 
 * The real operations properties can be overridden,
 * allowing several banking operations to be under the same date,
 * the same mode, ...
 * 
 * @author smouquet
 */
public class GroupedOperation extends UserOperation
{
    private List<BankingOperation> operations;
    
    private LocalDate date;
    private Mode mode;
    private String thirdParty;
    private String reference;
    
    private Set<String> tags = new LinkedHashSet<>();
    private String comment;
    
    
    public GroupedOperation() {}
    
    
    public List<BankingOperation> getOperations()
    {
        return operations;
    }
    
    public void setOperations(List<BankingOperation> operations)
    {
        this.operations = operations;
    }

    @Override
    public Account getAccount()
    {
        return operations.get(0).getAccount();
    }
    
    @Override
    public void setAccount(Account account)
    {
        operations.forEach(o -> o.setAccount(account));
    }

    @Override
    public LocalDate getDate()
    {
        return this.date;
    }

    @Override
    public void setDate(LocalDate date)
    {
        this.date = date;
    }

    @Override
    public Mode getMode()
    {
        return this.mode;
    }

    @Override
    public void setMode(Mode mode)
    {
        this.mode = mode;
    }

    @Override
    public String getThirdParty()
    {
        return this.thirdParty;
    }

    @Override
    public void setThirdParty(String thirdParty)
    {
        this.thirdParty = thirdParty;
    }

    @Override
    public String getReference()
    {
        return this.reference;
    }

    @Override
    public void setReference(String reference)
    {
        this.reference = reference;
    }

    @Override
    public BigDecimal getAmount()
    {
        return operations.stream().map(o -> o.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    
    @Override
    public void setAmount(BigDecimal amount)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<String> getTags()
    {
        return this.tags;
    }

    @Override
    public String getComment()
    {
        return this.comment;
    }
    
    @Override
    public void setComment(String comment)
    {
        this.comment = comment;
    }
}
