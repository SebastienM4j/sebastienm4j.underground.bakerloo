package xyz.smouquet.bakerloo.core.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A banking operation whose some properties can be overriden.
 * 
 * @author smouquet
 */
public class OverrideOperation extends UserOperation
{
    private BankingOperation operation;
    
    private LocalDate date;
    private Mode mode;
    private String thirdParty;
    private String reference;
    
    private Set<String> tags = new LinkedHashSet<>();
    private String comment;
    
    
    public OverrideOperation() {}

    
    public BankingOperation getOperation()
    {
        return operation;
    }
    
    public void setOperation(BankingOperation operation)
    {
        this.operation = operation;
    }
    
    @Override
    public Account getAccount()
    {
        return operation.getAccount();
    }
    
    @Override
    public void setAccount(Account account)
    {
        operation.setAccount(account);
    }

    @Override
    public LocalDate getDate()
    {
        return date != null ? date : operation.getDate();
    }

    @Override
    public void setDate(LocalDate date)
    {
        this.date = date;
    }

    @Override
    public Mode getMode()
    {
        return mode != null ? mode : operation.getMode();
    }

    @Override
    public void setMode(Mode mode)
    {
        this.mode = mode;
    }

    @Override
    public String getThirdParty()
    {
        return thirdParty != null ? thirdParty : operation.getThirdParty();
    }

    @Override
    public void setThirdParty(String thirdParty)
    {
        this.thirdParty = thirdParty;
    }
    
    @Override
    public String getReference()
    {
        return reference != null ? reference : operation.getReference();
    }
    
    @Override
    public void setReference(String reference)
    {
        this.reference = reference;
    }

    @Override
    public BigDecimal getAmount()
    {
        return operation.getAmount();
    }
    
    @Override
    public void setAmount(BigDecimal amount)
    {
        operation.setAmount(amount);
    }
    
    @Override
    public Set<String> getTags()
    {
        return !tags.isEmpty() ? tags : operation.getTags();
    }

    @Override
    public String getComment()
    {
        return comment != null ? comment : operation.getComment();
    }

    @Override
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    
}
