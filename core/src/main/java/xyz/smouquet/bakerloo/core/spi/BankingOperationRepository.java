package xyz.smouquet.bakerloo.core.spi;

import java.util.List;

import xyz.smouquet.bakerloo.core.domain.BankingOperation;

/**
 * Repository for {@link BankingOperation}.
 * 
 * @author smouquet
 */
public interface BankingOperationRepository extends CrudRepository<BankingOperation, Long>
{

    /**
     * Retrives all {@link BankingOperation} for an account
     * in a natural order of use.<br/>
     * <br/>
     * Operations are ordered by date and id (descending).
     * 
     * @param accountId Account id (non <tt>null</tt>)
     * @return All account banking operations in a natural order of use
     * @throws IllegalArgumentException If an argument is not valid
     */
    List<BankingOperation> findAllByAccountId(Long accountId);
    
}
