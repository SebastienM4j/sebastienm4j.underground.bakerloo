package xyz.smouquet.bakerloo.core.service;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;

import xyz.smouquet.bakerloo.core.api.BalanceService;
import xyz.smouquet.bakerloo.core.domain.BankingOperation;
import xyz.smouquet.bakerloo.core.spi.BalanceRepository;
import xyz.smouquet.bakerloo.core.spi.BankingOperationRepository;

/**
 * Service implementation for {@link BalanceService}.
 * 
 * @author smouquet
 */
@Named
public class BalanceServiceImpl implements BalanceService
{
    @Inject
    private Optional<BalanceRepository> balanceRepository;
    
    public void setBalanceRepository(Optional<BalanceRepository> balanceRepository)
    {
        this.balanceRepository = balanceRepository;
    }
    
    @Inject
    private Provider<BankingOperationRepository> bankingOperationRepositoryProvider;
    private BankingOperationRepository bankingOperationRepository;
    
    private BankingOperationRepository bankingOperationRepository()
    {
        if(bankingOperationRepository == null) {
            bankingOperationRepository = bankingOperationRepositoryProvider.get();
        }
        return bankingOperationRepository;
    }
    
    
    @Override
    public BigDecimal balanceByAccountId(Long accountId)
    {
        if(balanceRepository.isPresent()) {
            return balanceRepository.get().findBalanceByAccountId(accountId);
        }
        
        return calculateBalance(accountId, o -> true);
    }
    
    @Override
    public BigDecimal balanceCheckedOperationsByAccountId(Long accountId)
    {
        if(balanceRepository.isPresent()) {
            return balanceRepository.get().findBalanceCheckedOperationsByAccountId(accountId);
        }
        
        return calculateBalance(accountId, o -> o.isChecked());
    }
    
    @Override
    public BigDecimal balanceUncheckedOperationsByAccountId(Long accountId)
    {
        if(balanceRepository.isPresent()) {
            return balanceRepository.get().findBalenceUncheckedOperationsByAccountId(accountId);
        }
        
        return calculateBalance(accountId, o -> !o.isChecked());
    }
    
    
    private BigDecimal calculateBalance(Long accountId, Predicate<BankingOperation> filter)
    {
        List<BankingOperation> operations = bankingOperationRepository().findAllByAccountId(accountId);
        if(isEmpty(operations)) {
            return BigDecimal.ZERO;
        }
        
        return operations.stream()
                         .filter(filter)
                         .map(o -> o.getMode().isCredit() ? o.getAmount() : o.getAmount().negate())
                         .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

}
