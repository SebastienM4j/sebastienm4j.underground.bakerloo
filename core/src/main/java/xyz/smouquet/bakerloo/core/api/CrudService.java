package xyz.smouquet.bakerloo.core.api;

import java.io.Serializable;

/**
 * Service for generic CRUD operations on specific type.
 *
 * @param <T> Concerned type
 * @param <ID> Entity id
 * 
 * @author smouquet
 */
public interface CrudService<T, ID extends Serializable>
{

    /**
     * Retrieve an entity by his id.
     * 
     * @param id The entity id (non <tt>null</tt>)
     * @return The found entity or <tt>null</tt>
     * @throws IllegalArgumentException If an argument is not valid
     */
    T find(ID id);
    
    /**
     * Save the entity.
     * 
     * @param entity Entity to save (non <tt>null</tt>)
     * @return The saved entity
     * @throws IllegalArgumentException If an argument is not valid
     */
    T save(T entity);
    
    /**
     * Delete the entity that correspond to this id.
     * 
     * @param id The entity id (non <tt>null</tt>)
     * @throws IllegalArgumentException If an argument is not valid
     */
    void delete(ID id);
    
}
