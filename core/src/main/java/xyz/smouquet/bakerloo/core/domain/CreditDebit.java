package xyz.smouquet.bakerloo.core.domain;

/**
 * Credit and debit constants.
 * 
 * @author smouquet
 */
public enum CreditDebit
{
    CREDIT,
    DEBIT;
}
