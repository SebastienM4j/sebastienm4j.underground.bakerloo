package xyz.smouquet.bakerloo.core.api;

import xyz.smouquet.bakerloo.core.domain.UserOperation;

/**
 * Service for {@link UserOperation}.
 * 
 * @author smouquet
 */ 
public interface UserOperationService extends CrudService<UserOperation, Long>
{

}
