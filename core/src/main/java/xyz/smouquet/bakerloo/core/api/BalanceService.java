package xyz.smouquet.bakerloo.core.api;

import java.math.BigDecimal;

/**
 * Service that provides balance informations.
 * 
 * @author smouquet
 */
public interface BalanceService {

    /**
     * Get global balance for an account.
     * 
     * @param accountId Account id
     * @return The calculated balance
     */
    BigDecimal balanceByAccountId(Long accountId);

    /**
     * Get balance on checked operations only for an account.
     * 
     * @param accountId Account id
     * @return The calculated balance
     */
    BigDecimal balanceCheckedOperationsByAccountId(Long accountId);

    /**
     * Get balance on unchecked operations only for an account.
     * 
     * @param accountId Account id
     * @return The calculated balance
     */
    BigDecimal balanceUncheckedOperationsByAccountId(Long accountId);

}