/**
 * SPI (Service Provider Interfaces), API 
 * exposed for components that's the model need.
 * There are intended to be implemented or 
 * extended by a third party (provider).
 */
package xyz.smouquet.bakerloo.core.spi;