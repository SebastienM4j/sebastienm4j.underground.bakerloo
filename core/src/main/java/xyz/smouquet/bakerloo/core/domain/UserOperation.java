package xyz.smouquet.bakerloo.core.domain;

/**
 * This is a bank operation from the user perspective.
 * 
 * This can be a group of several real banking operations,
 * breakdown for statistical purposes, ...
 *  
 * @author smouquet
 */
public abstract class UserOperation implements Operation
{
    private Long id;
    
    
    @Override
    public Long getId()
    {
        return id;
    }
    
    @Override
    public void setId(Long id)
    {
        this.id = id;
    }
    
}
