package xyz.smouquet.bakerloo.core.domain;

import static xyz.smouquet.bakerloo.core.domain.CreditDebit.*;

/**
 * Banking operation mode.
 * 
 * @author smouquet
 */
public enum Mode
{
    CREDIT_CARD(DEBIT),
    WITHDRAWAL_ATM(DEBIT),
    EMITTED_CHECK(DEBIT),
    DEPOSIT_CHECK(CREDIT),
    LEVY(DEBIT),
    TRANSFER_ISSUED(DEBIT),
    TRANSFER_RECEIVED(CREDIT),
    PAYMENT(CREDIT);
    
    
    private CreditDebit cd;
    
    private Mode(CreditDebit cd)
    {
        this.cd = cd;
    }
    
    
    public boolean isCredit()
    {
        return cd.equals(CREDIT);
    }
    
    public boolean isDebit()
    {
        return cd.equals(DEBIT);
    }
}
