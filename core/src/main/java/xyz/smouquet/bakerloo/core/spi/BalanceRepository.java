package xyz.smouquet.bakerloo.core.spi;

import java.math.BigDecimal;

/**
 * Service that provides balance informations,
 * directly from persistence layer.
 * 
 * @author smouquet
 */
public interface BalanceRepository
{
    
    /**
     * Get global balance for an account.
     * 
     * @param accountId Account id
     * @return The calculated balance
     */
    BigDecimal findBalanceByAccountId(Long accountId);
    
    /**
     * Get balance on checked operations only for an account.
     * 
     * @param accountId Account id
     * @return The calculated balance
     */
    BigDecimal findBalanceCheckedOperationsByAccountId(Long accountId);
    
    /**
     * Get balance on unchecked operations only for an account.
     * 
     * @param accountId Account id
     * @return The calculated balance
     */
    BigDecimal findBalenceUncheckedOperationsByAccountId(Long accountId);
    
}
