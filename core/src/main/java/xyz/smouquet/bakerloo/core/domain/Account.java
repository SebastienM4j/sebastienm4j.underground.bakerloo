package xyz.smouquet.bakerloo.core.domain;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Represents a bank account.
 * 
 * @author smouquet
 */
public class Account
{
    private Long id;
    
    private String label;
    
    private String bankCode;
    private String officeCode;
    private String number;
    
    private Set<String> tags = new LinkedHashSet<>();
    
    
    public Account() {}
    

    public long getId()
    {
        return id;
    }
    
    public void setId(long id)
    {
        this.id = id;
    }
    
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    
    public Set<String> getTags() {
        return tags;
    }

}
