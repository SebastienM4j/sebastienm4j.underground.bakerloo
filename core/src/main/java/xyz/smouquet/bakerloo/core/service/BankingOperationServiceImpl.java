package xyz.smouquet.bakerloo.core.service;

import static org.apache.commons.lang3.Validate.notNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import xyz.smouquet.bakerloo.core.api.BankingOperationService;
import xyz.smouquet.bakerloo.core.domain.BankingOperation;
import xyz.smouquet.bakerloo.core.domain.BreakdownOperation;
import xyz.smouquet.bakerloo.core.domain.GroupedOperation;
import xyz.smouquet.bakerloo.core.domain.OverrideOperation;
import xyz.smouquet.bakerloo.core.domain.RawOperation;
import xyz.smouquet.bakerloo.core.domain.UserOperation;
import xyz.smouquet.bakerloo.core.spi.BankingOperationRepository;
import xyz.smouquet.bakerloo.core.spi.UserOperationRepository;

/**
 * Service implementation for {@link BankingOperationService}.
 * 
 * @author smouquet
 */
@Named
public class BankingOperationServiceImpl implements BankingOperationService
{
    @Inject
    private BankingOperationRepository bankingOperationRepository;
    @Inject
    private UserOperationRepository userOperationRepository;
    

    @Override
    public BankingOperation find(Long id)
    {
        notNull(id);
        
        return bankingOperationRepository.find(id);
    }

    /**
     * Save the {@link BankingOperation}.<br/>
     * <br/>
     * If the {@link BankingOperation} dont already exist, an {@link RawOperation}
     * is created to bind the operation with a {@link UserOperation}.
     */
    @Override
    @Transactional
    public BankingOperation save(BankingOperation entity)
    {
        notNull(entity);
        
        RawOperation userOperation = null;
        if(entity.getId() == null) {
            userOperation = new RawOperation();
        }
        
        BankingOperation saved = bankingOperationRepository.save(entity);
        
        if(userOperation != null) {
            userOperation.setOperation(saved);
            userOperationRepository.save(userOperation);
        }
        
        return saved; 
    }

    @Override
    @Transactional
    public void delete(Long id)
    {
        notNull(id);
        
        List<UserOperation> operations = userOperationRepository.findAllByBankingOperationId(id);
        if(isNotEmpty(operations))
        {
            operations.stream()
                      .filter(o -> (o instanceof RawOperation || o instanceof OverrideOperation || o instanceof BreakdownOperation))
                      .forEach(o -> userOperationRepository.delete(o.getId()));
            
            operations.stream()
                      .filter(o -> (o instanceof GroupedOperation))
                      .forEach(o -> updateGroupedOperationOnDelete(o, id));
        }
        
        bankingOperationRepository.delete(id);
    }
    
    private void updateGroupedOperationOnDelete(UserOperation o, Long bankingOperationId)
    {
        GroupedOperation operation = (GroupedOperation)o;
        
        Iterator<BankingOperation> it = operation.getOperations().iterator();
        while(it.hasNext())
        {
            if(it.next().getId().longValue() == bankingOperationId.longValue()) {
                it.remove();
                break;
            }
        }
        
        if(!operation.getOperations().isEmpty()) {
            userOperationRepository.save(operation);
        } else {
            userOperationRepository.delete(operation.getId());
        }
    }

}
