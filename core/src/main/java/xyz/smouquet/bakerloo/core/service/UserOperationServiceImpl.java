package xyz.smouquet.bakerloo.core.service;

import static org.apache.commons.lang3.Validate.notNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import xyz.smouquet.bakerloo.core.api.UserOperationService;
import xyz.smouquet.bakerloo.core.domain.BankingOperation;
import xyz.smouquet.bakerloo.core.domain.BreakdownOperation;
import xyz.smouquet.bakerloo.core.domain.GroupedOperation;
import xyz.smouquet.bakerloo.core.domain.OverrideOperation;
import xyz.smouquet.bakerloo.core.domain.RawOperation;
import xyz.smouquet.bakerloo.core.domain.UserOperation;
import xyz.smouquet.bakerloo.core.spi.BankingOperationRepository;
import xyz.smouquet.bakerloo.core.spi.UserOperationRepository;

/**
 * Service implementation for {@link UserOperationService}.
 * 
 * @author smouquet
 */
@Named
public class UserOperationServiceImpl implements UserOperationService
{
    @Inject
    private UserOperationRepository userOperationRepository;
    @Inject
    private BankingOperationRepository bankingOperationRepository;

    
    @Override
    public UserOperation find(Long id)
    {
        notNull(id);
        
        return userOperationRepository.find(id);
    }

    @Override
    @Transactional
    public UserOperation save(UserOperation entity)
    {
        notNull(entity);
        
        if(entity instanceof RawOperation)
        {
            RawOperation o = (RawOperation)entity;
            BankingOperation bo = o.getOperation();
            o.setOperation(bankingOperationRepository.save(bo));
        }
        else if(entity instanceof OverrideOperation)
        {
            OverrideOperation o = (OverrideOperation)entity;
            BankingOperation bo = o.getOperation();
            o.setOperation(bankingOperationRepository.save(bo));
        }
        else if(entity instanceof BreakdownOperation)
        {
            BreakdownOperation o = (BreakdownOperation)entity;
            BankingOperation bo = o.getOperation();
            o.setOperation(bankingOperationRepository.save(bo));
        }
        else if(entity instanceof GroupedOperation)
        {
            GroupedOperation o = (GroupedOperation)entity;
            List<BankingOperation> bos = o.getOperations();
            List<BankingOperation> savedBos = new ArrayList<>();
            bos.forEach(bo -> savedBos.add(bankingOperationRepository.save(bo)));
            o.setOperations(savedBos);
        }
        
        return userOperationRepository.save(entity);
    }

    @Override
    @Transactional
    public void delete(Long id)
    {
        notNull(id);
        
        UserOperation entity = userOperationRepository.find(id);
        
        if(entity instanceof RawOperation)
        {
            RawOperation o = (RawOperation)entity;
            BankingOperation bo = o.getOperation();
            bankingOperationRepository.delete(bo.getId());
        }
        else if(entity instanceof OverrideOperation)
        {
            OverrideOperation o = (OverrideOperation)entity;
            BankingOperation bo = o.getOperation();
            bankingOperationRepository.delete(bo.getId());
        }
        else if(entity instanceof BreakdownOperation)
        {
            BreakdownOperation o = (BreakdownOperation)entity;
            BankingOperation bo = o.getOperation();
            bankingOperationRepository.delete(bo.getId());
        }
        else if(entity instanceof GroupedOperation)
        {
            GroupedOperation o = (GroupedOperation)entity;
            List<BankingOperation> bos = o.getOperations();
            
            o.setOperations(new ArrayList<>());
            userOperationRepository.save(entity);
            
            bos.forEach(bo -> {
                RawOperation raw = new RawOperation();
                raw.setOperation(bo);
                userOperationRepository.save(raw);
            });
        }
        
        userOperationRepository.delete(id);
    }
    
}
