package xyz.smouquet.bakerloo.core.service;

import static org.apache.commons.lang3.Validate.notNull;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import xyz.smouquet.bakerloo.core.api.AccountService;
import xyz.smouquet.bakerloo.core.domain.Account;
import xyz.smouquet.bakerloo.core.domain.BankingOperation;
import xyz.smouquet.bakerloo.core.domain.UserOperation;
import xyz.smouquet.bakerloo.core.spi.AccountRepository;
import xyz.smouquet.bakerloo.core.spi.BankingOperationRepository;
import xyz.smouquet.bakerloo.core.spi.UserOperationRepository;

/**
 * Service implementation for {@link AccountService}.
 * 
 * @author smouquet
 */
@Named
public class AccountServiceImpl implements AccountService
{
    @Inject
    private AccountRepository accountRepository;
    @Inject
    private BankingOperationRepository bankingOperationRepository;
    @Inject
    private UserOperationRepository userOperationRepository;
    

    @Override
    public Account find(Long id)
    {
        notNull(id);
        
        return accountRepository.find(id);
    }

    @Override
    @Transactional
    public Account save(Account entity)
    {
        notNull(entity);
        
        return accountRepository.save(entity);
    }

    /**
     * Delete the account and all it's operations.
     */
    @Override
    @Transactional
    public void delete(Long id)
    {
        notNull(id);
        
        List<UserOperation> userOperations = userOperationRepository.findAllByAccountId(id);
        if(isNotEmpty(userOperations)) {
            userOperations.forEach(o -> userOperationRepository.delete(o.getId()));
        }
        
        List<BankingOperation> bankingOperations = bankingOperationRepository.findAllByAccountId(id);
        if(isNotEmpty(bankingOperations)) {
            bankingOperations.forEach(o -> bankingOperationRepository.delete(o.getId()));
        }
        
        accountRepository.delete(id);
    }

}
