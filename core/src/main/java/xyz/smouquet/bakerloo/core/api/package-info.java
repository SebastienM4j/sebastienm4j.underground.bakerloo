/**
 * API (Application Programming Interfaces) 
 * exposed for components needing the model.
 */
package xyz.smouquet.bakerloo.core.api;