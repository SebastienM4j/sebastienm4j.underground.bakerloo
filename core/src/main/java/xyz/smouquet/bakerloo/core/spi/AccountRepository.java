package xyz.smouquet.bakerloo.core.spi;

import xyz.smouquet.bakerloo.core.domain.Account;

/**
 * Repository for {@link Account}.
 * 
 * @author smouquet
 */
public interface AccountRepository extends CrudRepository<Account, Long>
{

}
