/**
 * Definition of the domain for the import of banking operations.
 * 
 * <p>Package is plural to avoid the Java keyword "import".
 */
package xyz.smouquet.bakerloo.imports.domain;