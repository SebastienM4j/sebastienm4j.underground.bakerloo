package xyz.smouquet.bakerloo.imports.spi;

import java.util.List;
import java.util.function.BiFunction;

import xyz.smouquet.bakerloo.core.domain.UserOperation;
import xyz.smouquet.bakerloo.imports.domain.SourceOperation;

/**
 * Function responsible to transform a {@link SourceOperation} into a {@link UserOperation}.
 * 
 * <p>The first function argument is the source of operation (the source to import).
 * The second is the previously transformed result. It could be <tt>null</tt> if no
 * transformation has been done.
 * 
 * <p>If this transformer is not affected by this source, he has to return the previous result
 * (the second argument) without any transformation.
 * 
 * <p>The result (and second argument) is a <code>List</code> because one source operations
 * can be transformed into several user operations.
 * 
 * @author smouquet
 */
@FunctionalInterface
public interface OperationTransformer extends BiFunction<SourceOperation, List<UserOperation>, List<UserOperation>>
{

}
