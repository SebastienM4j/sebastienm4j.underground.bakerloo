/**
 * API (Application Programming Interfaces) 
 * exposed for components needing the model.
 * 
 * <p>Package is plural to avoid the Java keyword "import".
 */
package xyz.smouquet.bakerloo.imports.api;