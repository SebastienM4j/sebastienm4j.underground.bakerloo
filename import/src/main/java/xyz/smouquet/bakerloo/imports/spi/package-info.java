/**
 * SPI (Service Provider Interfaces), API 
 * exposed for components that's the model need.
 * There are intended to be implemented or 
 * extended by a third party (provider).
 * 
 * <p>Package is plural to avoid the Java keyword "import".
 */
package xyz.smouquet.bakerloo.imports.spi;