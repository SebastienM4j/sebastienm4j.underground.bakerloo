package xyz.smouquet.bakerloo.imports.api;

import java.util.List;

import xyz.smouquet.bakerloo.imports.spi.OperationTransformer;
import xyz.smouquet.bakerloo.imports.spi.SourceOperationReader;

/**
 * Service for import banking operations
 * that are in an external format.
 * 
 * @author smouquet
 */
public interface ImportService
{

    /**
     * Launches import of banking operations.
     * 
     * <p>External operations are read by <code>reader</code>
     * (while {@link SourceOperationReader#read()} returns a <code>List</code>
     * of operations) and transformed by <code>transformers</code> ({@link OperationTransformer}
     * are called in the list order). Then, the service is responsible for registering new imported transactions.
     * 
     * @param reader Reader to load external operations to import (non <tt>null</tt>)
     * @param transformers Functions to transform a source operation before import (non <tt>null</tt>, not empty)
     */
    void importOperations(SourceOperationReader reader, List<OperationTransformer> transformers);
    
}
