package xyz.smouquet.bakerloo.imports.domain;

import xyz.smouquet.bakerloo.core.domain.Account;

/**
 * Mark a banking operation as a source for import.
 * This is an operation which is in an external format.
 * 
 * @author smouquet
 */
public interface SourceOperation
{
    
    /**
     * @return The {@link Account} for that source (non <tt>null</tt>)
     */
    Account getAccount();
    
}
