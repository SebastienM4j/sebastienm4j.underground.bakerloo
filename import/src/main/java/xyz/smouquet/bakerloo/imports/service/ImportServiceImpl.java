package xyz.smouquet.bakerloo.imports.service;

import static org.apache.commons.lang3.Validate.notNull;
import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import xyz.smouquet.bakerloo.core.api.UserOperationService;
import xyz.smouquet.bakerloo.core.domain.UserOperation;
import xyz.smouquet.bakerloo.imports.api.ImportService;
import xyz.smouquet.bakerloo.imports.domain.SourceOperation;
import xyz.smouquet.bakerloo.imports.spi.OperationTransformer;
import xyz.smouquet.bakerloo.imports.spi.SourceOperationReader;

/**
 * Service implementation for {@link ImportService}.
 * 
 * @author smouquet
 */
@Named
public class ImportServiceImpl implements ImportService
{
    @Inject
    private UserOperationService userOperationService;
    

    @Override
    @Transactional
    public void importOperations(SourceOperationReader reader, List<OperationTransformer> transformers)
    {
        notNull(reader);
        isTrue(isNotEmpty(transformers));
        
        List<SourceOperation> sourceOperations;
        while((sourceOperations = reader.read()) != null)
        {
            sourceOperations.stream()
                            .map(so -> transformSourceOperation(so, transformers))
                            .forEach(this::saveImportedOperation);
        }
    }
    
    
    private List<UserOperation> transformSourceOperation(SourceOperation sourceOperation, List<OperationTransformer> transformers)
    {
        List<UserOperation> result = null;
        for(OperationTransformer t : transformers) {
            result = t.apply(sourceOperation, result);
        }
        
        return result;
    }
    
    private void saveImportedOperation(List<UserOperation> importedOperation)
    {
        if(importedOperation != null) {
            importedOperation.forEach(io -> userOperationService.save(io));
        }
    }
    
}
