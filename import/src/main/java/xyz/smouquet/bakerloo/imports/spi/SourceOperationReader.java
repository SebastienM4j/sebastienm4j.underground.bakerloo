package xyz.smouquet.bakerloo.imports.spi;

import java.util.List;

import xyz.smouquet.bakerloo.imports.domain.SourceOperation;

/**
 * Component responsible for read operations
 * from an external source (in an external format).
 * 
 * @author smouquet
 */
public interface SourceOperationReader
{

    /**
     * Read a list of banking operations in 
     * an external format ({@link SourceOperation}).
     *  
     * @return List of operations to import or <tt>null</tt> if no more
     */
    List<SourceOperation> read();
    
}
